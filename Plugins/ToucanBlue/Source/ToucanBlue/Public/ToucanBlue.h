// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <thread>
#include <string>

#include <Runtime/Engine/Classes/Engine/Engine.h>

#include <HAL/RunnableThread.h>
#include <HAL/Runnable.h>

#include "CoreMinimal.h"

#include <Containers/UnrealString.h>
#include "Subsystems/EngineSubsystem.h"

#include "ToucanBlue.generated.h"

class FToucanModuleInterface : public IModuleInterface
{	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

class TCPMonogramThread;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FMonogramDataReceivedSignature, FName, rawOut, FName, operationOut, FName, inputOut, float, valueOut);

UCLASS()
class TOUCANBLUE_API UToucanBlue : public UEngineSubsystem
{
	GENERATED_BODY()
public:
	FRunnableThread* frunnablePointer;
	TCPMonogramThread* monogramThread;
	// Begin USubsystem
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	// End USubsystem

	UToucanBlue();
	~UToucanBlue();
	friend class TCPMonogramThread;
	//MonogramDataReceived is the outgoing blueprint event, it has no C++ implementation
	//we call it to notify blueprint code when there's a message.
	UPROPERTY(BlueprintAssignable, Category = "MonogramEvent")
	FMonogramDataReceivedSignature MonogramDataReceived;

};

class TCPMonogramThread : public FRunnable
{
public:
	TCPMonogramThread(UToucanBlue* parentBlueprint);
	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();
private:
	FThreadSafeCounter threadStop;
	UToucanBlue* parentBlue = nullptr;
	bool stop = false;
};
