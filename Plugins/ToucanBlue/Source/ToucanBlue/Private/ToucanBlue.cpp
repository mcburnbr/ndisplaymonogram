#include "ToucanBlue.h"
#include <chrono>
#include <cassert>
#include <locale>
#include <codecvt>

#include <Runtime/Core/Public/Async/Async.h>
#include <Runtime/Sockets/Public/Sockets.h>
#include <Runtime/Networking/Public/Interfaces/IPv4/IPv4SubnetMask.h>
#include <Runtime/Networking/Public/Interfaces/IPv4/IPv4Address.h>
#include <Runtime/Networking/Public/Interfaces/IPv4/IPv4Endpoint.h>
#include <Runtime/Networking/Public/Common/TcpSocketBuilder.h>
#include <Misc/CString.h>

#define bufferSize 100



#define LOCTEXT_NAMESPACE "UToucanBlue"



UToucanBlue::UToucanBlue() : UEngineSubsystem()
{

}

UToucanBlue::~UToucanBlue()
{
	delete monogramThread;
	monogramThread = nullptr;
}


TCPMonogramThread::TCPMonogramThread(UToucanBlue* parentBlueprint) : threadStop(0)
{

	this->parentBlue = parentBlueprint;
}

bool TCPMonogramThread::Init()
{
	return true;
}


void TCPMonogramThread::Stop()
{
	threadStop.Increment();
	return;
}

uint32 TCPMonogramThread::Run()
{
	UE_LOG(LogTemp, Warning, TEXT("ToucanBlue Thread started"));
	FSocket* Listener;
	FSocket* Connection;

	bool hasPending = false;
	bool done = false;

	int32 bytesRead = 0;
	FIPv4Address localHost(127, 0, 0, 1);
	Listener = FTcpSocketBuilder(TEXT("PaletteSocket")).AsReusable().AsBlocking().BoundToPort(47474).Listening(2);

	if (!Listener)
	{
		UE_LOG(LogTemp, Warning, TEXT("Toucan:<Fail> Couldn't create listener socket."));
		return 99;
	}

	while(0 == 0)
	{
		uint8 dataBuffer[bufferSize] = { 0 };
		bytesRead = 0;
		hasPending = false;
		UE_LOG(LogTemp, Warning, TEXT("ToucanBlue: Waiting for Connection!"));
		Listener->WaitForPendingConnection(hasPending, FTimespan::FromSeconds(6));

		if (hasPending == true)
		{

			UE_LOG(LogTemp, Warning, TEXT("Toucan: Connected to Turaco"));
			Connection = Listener->Accept(TEXT("Toucan: accepting incoming connection"));

			if (!Connection)
			{
				UE_LOG(LogTemp, Warning, TEXT("Toucan: <Fail> Couldn't accept Connection."));
				break;
			}

			FString carryOver = "";
			FString message = "";
			FString slashN = "\n";
			FString appended = "";
			while (threadStop.GetValue() == 0)
			{
				//if we have carry over and it contains a whole line just process that for now
				//otherwise read the socket
				if (carryOver.Contains("\n"))
				{
					bytesRead = 0;
					appended = carryOver;
				}
				else if (!Connection->Recv(dataBuffer, bufferSize, bytesRead, ESocketReceiveFlags::None))
				{
					UE_LOG(LogTemp, Warning, TEXT("Toucan: Connection to turaco lost, resetting socket."));
					break;
				}

				//if we have no data read skip this and assume we're just processing carryover
				if (bytesRead > 0)
				{
					int32 inCount = bytesRead;
					FString recvString(inCount, (TCHAR*)ANSI_TO_TCHAR((ANSICHAR*)dataBuffer));
					appended = carryOver.Append(recvString);
				}

				if (appended.Contains("\n"))
				{
					if (!appended.Split(slashN, &message, &carryOver))
					{
						UE_LOG(LogTemp, Warning, TEXT("Toucan: Nothing to split, this shouldn't happen."), *message);
						continue;
					}
				}
				else
				{
					//this should only happen if we didn't read *enough* so we don't have a line yet
					continue;
				}

				FString left;
				FString right;
				FString plus = "+";
				FString equals = "=";
				FString operation = "";
				float parseFloat = 0.0f;
				if (message.Split(plus, &left, &right))
				{
					parseFloat = FCString::Atof(*right);
					operation = plus;
				}
				else
				{
					if (message.Split(equals, &left, &right))
					{
						parseFloat = FCString::Atof(*right);
						operation = equals;
					}
				}
				FName messageSend = FName(*message);
				FName inputNameSend = FName(*left);
				FName operationTypeSend = FName(*operation);

				if (inputNameSend.Compare("") == 0) {
					inputNameSend = messageSend;
				}

				if (operationTypeSend.Compare("None") == 0)
				{
					message.Split("\r", &left, &right);
					inputNameSend = FName(*left);
				}

				//I'm not sure if it's an issue to call blueprintRunable functions from another thread.
				//So to be safe we'll post it to the main thread instead.
				AsyncTask(ENamedThreads::GameThread, [&]()
				{
					//this is running (with a reference capture on message, which doesn't exist if this function exits).

					if (parentBlue->MonogramDataReceived.IsBound())
					{
						parentBlue->MonogramDataReceived.Broadcast(messageSend, operationTypeSend, inputNameSend, parseFloat);
					}
				});

				UE_LOG(LogTemp, Warning, TEXT("%s"), *message);
			}
			Connection->Close();
			delete Connection;
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Exiting."));
	delete Listener;
	return 0;
}

void UToucanBlue::Initialize(FSubsystemCollectionBase& Collection)
{
	monogramThread = new TCPMonogramThread(this);
	frunnablePointer = FRunnableThread::Create(monogramThread, TEXT("TCP watcher thread"), 0);
	UE_LOG(LogTemp, Warning, TEXT("Startup complete!"));
}

void UToucanBlue::Deinitialize()
{
	if (monogramThread != nullptr)
	{
		delete monogramThread;
		monogramThread = nullptr;
	}
}

void FToucanModuleInterface::StartupModule()
{

}

void FToucanModuleInterface::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}


#undef LOCTEXT_NAMESPACE
IMPLEMENT_MODULE(FToucanModuleInterface, ToucanBlue)
